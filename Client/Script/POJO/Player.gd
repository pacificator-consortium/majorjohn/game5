class_name Player

var player_id: int
var name: String
var color: Color
var score: int

func _init(player_id_param: int, name_param: String, color_param: String, score_param: int):
	self.player_id = player_id_param
	self.name = name_param
	self.color = Color.html(color_param)
	self.score = score_param
	
static func create_from_json(player: Dictionary) -> Player:
	return Player.new(player["player_id"], player["name"], player["color"], player["score"])
