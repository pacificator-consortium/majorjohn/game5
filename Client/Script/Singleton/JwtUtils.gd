class_name JwtUtils


static func decode_jwt_payload(jwt_token: String) -> Dictionary:
	var parts = jwt_token.split(".")
	if parts.size() != 3:
		print("Le token JWT n'est pas valide.")
		return {}
	
	var payload_encoded = parts[1]
	var payload_json = Marshalls.base64_to_utf8(payload_encoded)
	var payload_dict = {}
	var json = JSON.new()
	var result = json.parse(payload_json)
	if result == OK:
		payload_dict = json.get_data()
	else:
		print("Impossible de parser le payload JSON.")
	return payload_dict
