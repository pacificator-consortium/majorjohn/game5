extends Node

signal wait_for_color
signal color_selected(player_name: String, player_color: Color)
signal start_game(player_name: String, player_color: Color)

var jwt_token

func _ready():
	color_selected.connect(_on_color_selected)

func _on_color_selected(player_name: String, player_color: Color):
	get_new_token.rpc(player_name, player_color)

func start_connection_sequence(player_name: String) -> void:
	jwt_token = ConfigReader.load_token(player_name)
	check_version.rpc(ConfigReader.load_version())
	

@rpc("any_peer", "call_remote", "reliable")
func validate_token_response(is_valid_token: bool) -> void:
	if is_valid_token:
		var payload = JwtUtils.decode_jwt_payload(jwt_token)
		var player_name: String = payload["player_name"]
		var player_color: Color = Color.html(payload["player_color"])
		start_game.emit(player_name, player_color)
		PlayerManager.connect_player.rpc(multiplayer.get_unique_id(), player_name, player_color)
	else:
		OS.alert("Failed to connect. Your player token is invalid")

@rpc("any_peer", "call_remote", "reliable")
func validate_token(_jwt_token: String) -> void:
	print("validate_token rpc")
	pass

@rpc("any_peer", "call_remote", "reliable")
func get_new_token(_player_name: String, _player_color: Color) -> void:
	print("get_new_token rpc")
	pass
	
@rpc("any_peer", "call_remote", "reliable")
func get_new_token_response(jwt_token_param) -> void:
	print("Generated token " + str(jwt_token_param))
	if(jwt_token_param == null):
		OS.alert("Failed to connect. This pseudo is already used")
	else:
		jwt_token = jwt_token_param
		ConfigReader.save_token(jwt_token_param)
		validate_token.rpc(jwt_token)
		
	

@rpc("any_peer", "call_remote", "reliable")
func check_version(_version: String) -> void:
	print("check_version rpc")
	pass

@rpc("any_peer", "call_remote", "reliable")
func check_version_response(server_version: String) -> void:
	if server_version == "":
		# Server has accept the connection
		if jwt_token == null:
			wait_for_color.emit()
		else: 
			validate_token.rpc(jwt_token)
	else:
		OS.alert("Failed to connect. expected version: %s current client version: %s" % [server_version, ConfigReader.load_version()])
