extends Node

signal object_selected(selectable: Selectable)

static var last_selection: Selectable

func select(new_selection: Selectable):
	if new_selection != last_selection:
		if last_selection != null:
			last_selection.set_selection(false)
		new_selection.set_selection(true)
		last_selection = new_selection
	else:
		# Unselect the object
		new_selection.set_selection(false)
		last_selection = null
	object_selected.emit(last_selection)
