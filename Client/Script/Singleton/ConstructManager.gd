extends Node

signal construction_mode(active: bool)

var is_constuction_mode_active: bool

func _ready():
	construction_mode.connect(_on_construction_mode)

func _on_construction_mode(active: bool):
	is_constuction_mode_active = active
