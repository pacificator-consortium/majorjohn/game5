extends Node

signal spawn_new_ship

var players: Array = []
var is_populated: bool = false

func get_player_by_player_id(player_id: int):
	for player: Player in players:
		if player.player_id == player_id:
			return player
	return null



@rpc("any_peer", "call_remote", "reliable")
func connect_player(_id: int, _player_name: String, _player_color: Color) -> void:
	print("connect_player rpc")

## Called durring the client connection
@rpc("authority", "call_remote", "reliable")
func populate_player_list(json_string: String) -> void:
	print("populate_player_list " + json_string)
	is_populated = true
	var json = JSON.new()
	var error = json.parse(json_string)
	if error == OK:
		for player in json.data:
			players.append(Player.create_from_json(player))
	else:
		print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())

## Called durring the client connection if the player has no ship
@rpc("authority", "call_remote", "reliable")
func player_is_dead() -> void:
	print("player_is_dead")
	Network.player_ship_destroy.emit()

## Called when a new player connect
@rpc("any_peer", "call_remote", "reliable")
func new_player_connect(json_string: String) -> void:
	if(is_populated):
		print("new_player_connect " + json_string)
		var json = JSON.new()
		var error = json.parse(json_string)
		if error == OK:
			var player = Player.create_from_json(json.data)
			#if players.filter(func(p): return p.name == player.name).is_empty():
			players.append(player)
		else:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())

## Called when a player is upadated
@rpc("any_peer", "call_remote", "reliable")
func update_player(json_string: String) -> void:
	if(is_populated):
		print("update_player " + json_string)
		var json = JSON.new()
		var error = json.parse(json_string)
		if error == OK:
			var player = Player.create_from_json(json.data)
			for filter_player in players.filter(func(p): return p.name == player.name):
				filter_player.score = player.score
		else:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
