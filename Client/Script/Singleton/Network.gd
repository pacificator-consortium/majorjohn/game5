extends Node

signal player_ship_destroy


func connect_to_server(host: String, player_name: String):
	print("_ready Network")
	var peer = ENetMultiplayerPeer.new()
	const port: int = 5555
	print("Try to connect To: " + host + ":" + str(port))
	peer.create_client(host, port)
	multiplayer.connection_failed.connect(connection_lost)
	multiplayer.connected_to_server.connect(func (): connection_successful(player_name))
	multiplayer.multiplayer_peer = peer

func connection_lost():
	OS.alert("Failed to connect.")

func connection_successful(player_name: String):
	print("Connected !")
	ConnectionManager.start_connection_sequence(player_name)


