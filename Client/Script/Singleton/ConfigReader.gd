class_name ConfigReader


static func load_version() -> String:
	var file = FileAccess.open("res://Config/version.txt", FileAccess.READ)
	var version = file.get_as_text()
	return version

static func load_token(player_name: String):
	var token_file_path: String = "user://%s.token" % player_name
	if FileAccess.file_exists(token_file_path):
		var file = FileAccess.open(token_file_path, FileAccess.READ)
		return file.get_as_text()
	else:
		return null

static func save_token(jwt_token: String):
	var payload = JwtUtils.decode_jwt_payload(jwt_token)
	var token_file_path: String = "user://%s.token" % payload["player_name"]
	var file = FileAccess.open(token_file_path, FileAccess.WRITE)
	file.store_string(jwt_token)
