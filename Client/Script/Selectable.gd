## Use to detect object selectable
class_name Selectable

const selector_scene = preload("res://GameScene/Prefab/Selector/Selector.tscn")
const margin: int = 30

var selected: bool = false
var selector_nodes: Array = []

## Automatise the init of selector node
## ref_node : node where selectors node will be add
## size : the size of the node (use texture get_size()
func init_selector(ref_node: Node2D, size: Vector2):
	var left_top_selector = selector_scene.instantiate()
	left_top_selector.position = Vector2(-margin - (size.x / 2) , -margin - (size.y / 2))
	left_top_selector.visible = false
	ref_node.add_child(left_top_selector)
	selector_nodes.append(left_top_selector)
	
	var right_top_selector = selector_scene.instantiate()
	right_top_selector.rotation_degrees = 90
	right_top_selector.position = Vector2(margin + (size.x / 2), -margin - (size.y / 2))
	right_top_selector.visible = false
	ref_node.add_child(right_top_selector)
	selector_nodes.append(right_top_selector)
	
	var left_bottom_selector = selector_scene.instantiate()
	left_bottom_selector.rotation_degrees = 180
	left_bottom_selector.position = Vector2(margin + (size.x / 2), margin + (size.y / 2))
	left_bottom_selector.visible = false
	ref_node.add_child(left_bottom_selector)
	selector_nodes.append(left_bottom_selector)
	
	var right_bottom_selector = selector_scene.instantiate()
	right_bottom_selector.rotation_degrees = 270
	right_bottom_selector.position = Vector2(-margin - (size.x / 2) , margin + (size.y / 2))
	right_bottom_selector.visible = false
	ref_node.add_child(right_bottom_selector)
	selector_nodes.append(right_bottom_selector)
	

func _init():
	pass
	
func invert_selection():
	set_selection(!self.selected)

func is_selected() -> bool:
	return selected

## Override this method to handle when an object is selected or unselected
func set_selection(selected_param: bool):
	self.selected = selected_param
	for selector in selector_nodes:
		selector.visible = self.selected

## Override this method to handle what an object show when selected
## Should return a dictionnary 
func get_selection_info() -> Dictionary:
	return {}

## Override this method to handle what an object show when selected, refresh all time
## Should return a dictionnary
func get_selection_info_refreshed() -> Dictionary:
	return {}
