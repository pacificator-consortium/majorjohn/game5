extends Timer

class_name CooldownNode

var cooldown: bool = true

# Called when the node enters the scene tree for the first time.
func _ready():
	self.timeout.connect(_reset_cooldown)

func run_cooldown():
	if cooldown:
		cooldown = false
		self.start()

func _reset_cooldown():
	self.cooldown = true


