extends ColorRect

class_name ColorSelection

var selectable: Selectable
@onready var selection_rect = get_node("SelectionRect2")
@onready var colors_selector_node = get_parent()

func _ready():
	selectable = ColorSelectable.new(self)

func _input(event):
	if event is InputEventMouseButton and event.is_pressed() and get_global_rect().has_point(event.position):
		# Use signal to replace get_parent() ?
		for i: ColorSelection in colors_selector_node.get_children():
			i.selectable.set_selection(false)
		selectable.invert_selection()
