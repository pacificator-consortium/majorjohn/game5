class_name ColorSelectable

extends Selectable

var color_selection: ColorSelection

func _init(color_selection_param: ColorSelection):
	self.color_selection = color_selection_param
	
func set_selection(selected_param: bool):
	super(selected_param)
	color_selection.selection_rect.visible = selected_param
	color_selection.colors_selector_node.selected_color = color_selection.color
