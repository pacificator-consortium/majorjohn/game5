extends MultiplayerSpawner


const RocketScene = preload("res://GameScene/Prefab/Weapon/Rocket.tscn")

func _enter_tree() -> void:
	spawn_function = _spawn_rocket

func _spawn_rocket(_id: int):
	#print("_spawn_rocket")
	var rocket_node := RocketScene.instantiate()
	return rocket_node

@rpc("any_peer", "call_remote", "reliable")
func add_rocket(_direction: Vector2, _starting_position: Vector2) -> void:
	#print("add_rocket rpc")
	pass


func _on_player_ship_fire_weapon(direction: Vector2, starting_position: Vector2, ship_rotation: float):
	#print("_on_player_ship_fire_weapon")
	add_rocket.rpc(direction, starting_position, ship_rotation)
