extends Area2D

class_name PlayerShip

signal fire_weapon

@onready var sprite_node: Sprite2D = $PlayerShipSprite2D
@onready var construct_area_node: Area2D = $ConstructArea2D
@onready var fire_timer_node: CooldownNode = $FireCooldownNode
@onready var construct_timer_node: CooldownNode = $ConstructCooldownNode
@onready var rocket_spawner_node: MultiplayerSpawner = $/root/MainRootGame/RocketMultiplayerSpawner
@onready var plot_spawner_node: MultiplayerSpawner = $/root/MainRootGame/PlotMultiplayerSpawner

@export var owner_id: int
@export var player_id: int

var selectable: ShipSelectable

@export var fire_rate_second = 0.5
@export var construct_rate_second = 5

# Temporaire pour le PoC d'anim
@export var destination: Vector2
@onready var propulsor_anim_left: Sprite2D = $"Modular-ship/Modular-propulsor-anim-left"
@onready var propulsor_anim_right: Sprite2D = $"Modular-ship/Modular-propulsor-anim-right"

func _process(delta):
	# Y'a plus optimal uniquement pour le PoC d'anim
	show_anim(destination != null and position != destination)


func _ready():
	selectable = ShipSelectable.new(self)
	fire_weapon.connect(rocket_spawner_node._on_player_ship_fire_weapon)
	fire_timer_node.wait_time = fire_rate_second
	construct_timer_node.wait_time = construct_rate_second
	ConstructManager.construction_mode.connect(_on_construct_mode)
		
@rpc("any_peer", "call_remote", "reliable")
func set_destination(_destination_param: Vector2):
	#print("set_destination rpc")
	pass

func _input(_event):
	if Input.is_action_pressed("move_to") and selectable.is_selected() and owner_id == multiplayer.get_unique_id():
		set_destination.rpc(get_global_mouse_position())
	if Input.is_action_pressed("fire") and selectable.is_selected() and owner_id == multiplayer.get_unique_id():
		if fire_timer_node.cooldown:
			fire_timer_node.run_cooldown()
			fire_weapon.emit(get_global_mouse_position(), position, rotation)

	
func _on_construct_mode(toogled_on: bool):
	if owner_id == multiplayer.get_unique_id():
		construct_area_node.visible = toogled_on

func _on_input_event(_viewport, _event, _shape_idx):
	if Input.is_action_pressed("select"):
		SelectorManager.select(self.selectable)

func _on_tree_exiting():
	if owner_id == multiplayer.get_unique_id():
		Network.player_ship_destroy.emit()


func _on_construct_area_2d_input_event(viewport, event, shape_idx):
	if owner_id == multiplayer.get_unique_id() and Input.is_action_pressed("move_to") and ConstructManager.is_constuction_mode_active:
		if construct_timer_node.cooldown:
			construct_timer_node.run_cooldown()
			plot_spawner_node.add_plot.rpc(get_global_mouse_position())

func show_anim(visible_param: bool):
	propulsor_anim_left.visible = visible_param
	propulsor_anim_right.visible = visible_param
