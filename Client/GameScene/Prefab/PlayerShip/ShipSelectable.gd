class_name ShipSelectable

extends Selectable

var player_ship: PlayerShip

func _init(player_ship_param: PlayerShip):
	self.player_ship = player_ship_param
	super.init_selector(player_ship, player_ship.sprite_node.texture.get_size())
	

func get_selection_info() -> Dictionary:
	var player: Player = PlayerManager.get_player_by_player_id(self.player_ship.player_id)
	var owner_name = ""
	if (player != null):
		owner_name = player.name
	return { 
		"Owner" : owner_name
	}
	
func get_selection_info_refreshed() -> Dictionary:
	return {
		"fire_cooldown": str(player_ship.fire_timer_node.cooldown),
		"construct_cooldown": str(player_ship.construct_timer_node.cooldown)
	}
