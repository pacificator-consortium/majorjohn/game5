extends Sprite2D


@onready var ship_node: PlayerShip = get_parent()
@export var grue_rotation_speed = 0.1

func _process(delta):
	# TODO : Optimise ?
	if ConstructManager.is_constuction_mode_active and ship_node.selectable.is_selected() and ship_node.owner_id == multiplayer.get_unique_id():
		rotate_towards_target(self, get_global_mouse_position(), grue_rotation_speed)


func rotate_towards_target(object_to_rotate, target, rotation_speed):
	var target_angle = object_to_rotate.global_position.angle_to_point(target)
	var ship_rotation = fmod(ship_node.rotation, TAU)
	object_to_rotate.rotation = lerp_angle(object_to_rotate.rotation, target_angle - ship_rotation, rotation_speed)
