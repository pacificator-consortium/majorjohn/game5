extends ItemList

const column_1_title: String = "Player"
const column_2_title: String = "Score"
const column_3_title: String = "Team"
const tileScene = preload("res://Widget/ColorTile/ColorTile.tscn")

func reset_board():
	self.clear()
	self.add_item(column_1_title, null, false)
	self.add_item(column_2_title, null, false)
	self.add_item(column_3_title, null, false)

func _on_visibility_changed():
	if self.visible:
		# Compute the table
		for player in PlayerManager.players:
			self.add_item(player.name, null, false)
			self.add_item(str(player.score), null, false)
			var tile = tileScene.instantiate()
			tile.init(player.color)
			#TODO setup player color	
			self.add_item(player.color.to_html(), tile, false)

func _input(_event):
	if Input.is_action_just_pressed("score_board"):
		reset_board()
		self.visible = true
	if Input.is_action_just_released("score_board"):
		self.visible = false
