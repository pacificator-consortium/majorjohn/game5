extends Node

@onready var root_control_node = $CanvasLayer/CanvasLayerControlRoot
@onready var color_node = $CanvasLayer/CanvasLayerControlRoot/PlayerVBox/ColorHBox/ColorPlayer
@onready var pseudo_node = $CanvasLayer/CanvasLayerControlRoot/PlayerVBox/NicknameHBox/LabelNicknameText

func setPseudo(player_name):
	pseudo_node.text = player_name
	
func setColor(player_color):
	color_node.color = player_color



func _on_visibility_changed():
	root_control_node.visible = self.visible
