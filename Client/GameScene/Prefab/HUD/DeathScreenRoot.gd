extends Control

@onready var death_timer_label_node: Label = $DeathTimerLabel
@onready var death_timer_node: Timer = $DeathTimer
@onready var respawn_button_node: Button = $RespawnButton
var respawn_timer_value_second: int = 10
var timer_iterator: int = 0

func _ready():
	death_timer_node.timeout.connect(timer_end)
	Network.player_ship_destroy.connect(run_death_screen)

func run_death_screen():
	respawn_button_node.disabled = true
	self.visible = true
	timer_iterator = 0
	update_timer_text()
	death_timer_node.start()
	
func timer_end():
	if timer_iterator == respawn_timer_value_second:
		update_timer_text()
		respawn_button_node.disabled = false
	else :
		timer_iterator = timer_iterator + 1
		update_timer_text()
		death_timer_node.start()
	
func update_timer_text():
	death_timer_label_node.text = str(respawn_timer_value_second - timer_iterator)


func _on_respawn_button_pressed():
	self.visible = false
	PlayerManager.spawn_new_ship.emit()
