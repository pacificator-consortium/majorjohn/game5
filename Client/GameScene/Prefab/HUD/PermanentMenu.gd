extends Control



func _on_construct_button_toggled(toggled_on: bool):
	ConstructManager.construction_mode.emit(toggled_on)
