extends ItemList

var index_item: Dictionary = {}
var selected_ready: bool = false


func _ready():
	SelectorManager.object_selected.connect(_on_object_selected)

func _process(delta):
	# TODO Eurk, replace by local timer
	if(SelectorManager.last_selection == null):
		selected_ready = false
	if(SelectorManager.last_selection != null && selected_ready):
		var dict: Dictionary = SelectorManager.last_selection.get_selection_info_refreshed()
		for key in dict:
			self.set_item_text(index_item[key], dict[key])

func _on_object_selected(selectable: Selectable):
	self.clear()
	if (selectable != null):
		var dict: Dictionary = selectable.get_selection_info()
		for key in dict:
			self.add_item(key, null, false)
			self.add_item(dict[key], null, false)
		dict = selectable.get_selection_info_refreshed()
		for key in dict:
			self.add_item(key, null, false)
			index_item[key] = self.add_item(dict[key], null, false)
		selected_ready = true


