extends Area2D

class_name Planet

@onready var sprite_node: Sprite2D = $PlanetSprite2D
@export var planet_id: int

var selectable: PlanetSelectable

func _ready():
	selectable = PlanetSelectable.new(self)


func _on_input_event(_viewport, _event, _shape_idx):
	if Input.is_action_pressed("select"):
		SelectorManager.select(self.selectable)
