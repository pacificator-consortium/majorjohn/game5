class_name PlanetSelectable

extends Selectable

var planet: Planet

func _init(planet_param: Planet):
	self.planet = planet_param
	super.init_selector(planet, planet.sprite_node.texture.get_size())
	

func get_selection_info() -> Dictionary:
	return { 
		"id" : str(planet.planet_id)
	}
