extends MultiplayerSpawner

class_name PlanetMultiplayerSpawner

const PlanetScene = preload("res://GameScene/Prefab/Planet/Planet.tscn")
	
func _enter_tree() -> void:
	spawn_function = _spawn_planet

func _spawn_planet(id: int) -> Planet: 
	print("_spawn_planet id: " + str(id))
	return PlanetScene.instantiate()

