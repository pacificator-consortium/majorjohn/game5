extends Node

#var player_color
#var player_name

@onready var hud_node = $HUD

func _ready():
	ConnectionManager.start_game.connect(_on_start_game)
	
func _on_start_game(player_name: String, player_color: Color): 
	print("_on_start_game (MainScene)")
	hud_node.setPseudo(player_name)
	hud_node.setColor(player_color)
