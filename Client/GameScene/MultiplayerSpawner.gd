extends MultiplayerSpawner

@onready var main_scene_node = $/root/MainRootGame

const PlayerScene = preload("res://GameScene/Prefab/PlayerShip/PlayerShip.tscn")

func _enter_tree() -> void:
	spawn_function = _spawn_ship

func _spawn_ship(id: int):
	print("_spawn_ship id: " + str(id) )
	var player_node := PlayerScene.instantiate()
	return player_node

func _ready():
	PlayerManager.spawn_new_ship.connect(func(): self.spawn_new_ship.rpc())


@rpc("any_peer", "call_remote", "reliable")
func spawn_new_ship() -> void:
	print("spawn_new_ship rpc")
