extends MultiplayerSpawner


class_name PlotMultiplayerSpawner

const PlotScene = preload("res://GameScene/Prefab/Plot/Plot.tscn")

func _enter_tree() -> void:
	spawn_function = _spawn_plot

func _spawn_plot(id: int) -> Planet: 
	print("_spawn_plot id: " + str(id))
	return PlotScene.instantiate()
	
@rpc("any_peer", "call_remote", "reliable")
func add_plot(starting_position: Vector2) -> void:
	print("add_plot rpc")
