extends Node


@onready var host_node: TextEdit = $/root/MainRootSelection/ServerSelection/HostTextEdit
@onready var player_name_node: TextEdit = $/root/MainRootSelection/ServerSelection/TextEditPseudo
@onready var color_selection_node: Control = $/root/MainRootSelection/ColorSelection
@onready var color_selector_node: Control = $/root/MainRootSelection/ColorSelection/ColorsSelector
@onready var server_selection_node: Control = $/root/MainRootSelection/ServerSelection
@onready var main_root_selection_node: = $/root/MainRootSelection
@onready var root_node: = $/root


var main_root_game_node: Node2D


func _ready():
	ConnectionManager.wait_for_color.connect(_on_color_need)
	ConnectionManager.start_game.connect(load_game_scene)


func load_game_scene(player_name: String, player_color: Color): 
	main_root_selection_node.visible = false
	main_root_game_node.visible = true



func _on_button_connect_pressed():
	main_root_game_node = load("res://GameScene/MainScene.tscn").instantiate();
	main_root_game_node.visible = false
	root_node.add_child(main_root_game_node)
	
	var host_address = host_node.get_text()
	var player_name = player_name_node.get_text()
	
	Network.connect_to_server(host_address, player_name)
	
func _on_color_need():
	server_selection_node.visible = false
	color_selection_node.visible = true


func _on_button_start_pressed():
	print("Click !")
	ConnectionManager.color_selected.emit(player_name_node.get_text(), color_selector_node.selected_color)
