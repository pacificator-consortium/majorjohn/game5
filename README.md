# Game5

Requière: Godot 4.2.2

Test d'implémentation d'un jeu sur Godot.

L'idée part d'une tentative de génération d'un jeu avec ChatGPT avec le prompt suivant :

```
Génère moi le code en unity c# d'un jeu 2d ou l'on controlle un nombre de vaisseau entre 1 et 10.

Le jeu commence par une page d'acceuille ou l'on choisis un pseudo et une couleur. La couleur permettra d'identifier les vaisseaux du joueur.

Chaque vaisseau appartenant au joueur est déplaçable. Le joueur peut selectionner un vaisseau avec un clic droit. Pour déplacer un vaisseau on utilise un clic gauche. Un joueur ne peut pas déplacer les vaisseaux d'un autre joueur.

Chaque vaisseau posède 10 points de vie. Lorsque deux vaisseau n'appartenant pas au même joueur sont proche, ils perdent chacun un point de vie.

 Le jeu est multijoueur.

La carte est dans des dimensions limités. 
```

# Lancement :

Le jeu tourne sur le port 8080.

Pour générer un serveur, dans Godot : Projet -> Exporter... -> Exporter le projet... (Configuration Windows Desktop). Lancer le serveur via un shell avec le paramêtre --headless. Cela permet de lancer le serveur dédier.

Pour lancer le jeu utiliser directement l'éditeur. Pour lancer plusieur instance dans Godot : Débogage -> Exécuter plusieur instance

Le jeu sauvegarde la carte ici : %appdata%\Godot\app_userdata\Test

# Docker
## CI/CD

Les variables DOCKER_USER et DOCKER_PASSWORD doivent être défini dans le projet au niveau [CI/CD](https://gitlab.com/pacificator-consortium/majorjohn/sword/swordservices/-/settings/ci_cd)

La branche doit être "sécurisé" pour faire tourner la pipeline dessus (Verification gitlab). La configuration se fait [ici](https://gitlab.com/pacificator-consortium/majorjohn/game5-group/game5/-/settings/repository#js-branch-rules)

Les tags de version doivent être en lower-case

## Utilisation du repository

Le repository docker privé s'appelle : majorjohn/draftcorporation-repository

Il est necessaire que l'image s'appelle pareil. on utilisera des tags custom pour pousser différente image.
Sous le format :

```
[nom-de-l-image]-x.x
```

Commande pour se logger pour le repository :

```
docker login -u "hubusername" -p "password" docker.io
```

Pour pousser l'image docker :
```
docker push majorjohn/draftcorporation-repository:tagname
```

## Creation d'une image en local

Pour creer une image en local utiliser la commande :
```
# Dans le dossier Server
docker build . -t game5-server-local -f docker/Dockerfile-game5server.dockerfile
# Ou dans le dossier Server
docker-compose build
```

## Deploiement sur le server de prod

Bien penser à s'authentifier sur le repository avant de lancer la commande
```
# Login
docker login -u "hubusername" -p "password" docker.io
# Run (Should pull the right version)
docker-compose up --detach
```



## TODO :

- [ ] Point de spawn
- [ ] Remplir le vide du monde
- [ ] Voir si un joueur est en ligne dans le tableau des scores
- [ ] Gérer le full screen
- [ ] Update Godot 4.3
- [ ] Bouton pour centrer sur le joueur
- [ ] Centrer la caméra sur le joueur au départ

# Done (No version associate):
- [X] Si même couleurs les roquettes traverse
- [X] Respawn avec timer
- [X] Mettre un timer pour le tir

# 0.2 - 11/11/2024
- [X] Sécurisation du pseudo par un token local
- [X] Build du client automatique
- [X] La selection d'un objet permet de voir a qui appartient l'objet
- [X] Changement de la section surbriance -> un réticule autour de la cible
- [X] Système de version (Refus d'un client dans la mauvaise version)
- [X] Persistance
- [X] Tableau des scores

# 0.3 - WIP

- [ ] Mécanique de tir
    - [ ] Faire partir les missiles de côté
    - [ ] Courbe pas du tout intuitive
- [ ] Ajout de NPC
    - [X] NPC
    - [ ] Trajectoire
- [ ] Popup pour description

### Pas reussi :
- [ ] Voir la couleur des joueurs dans le tableau des scores

## Technique hypothétique :
- [ ] Popup in game
- [ ] En cas d'erreur, retour au menu principal
- [ ] Pimp menu principal

## Fonctionnalité hypothétique :
- [ ] Selection d'arme
- [ ] ~~Ligne d'énergie (Necessaire au déplacement)~~
```
L'interêt de la ligne énergétique (Créer de l'interaction autour d'un point) semble trop faible en terme de gameplay. Mis en pause de cette aspect, prévoir un nettoyage si solution déprécié.
```
- [ ] Brouillard de guerre

## Bug :
- [ ] Lors de la selection de la couleur, il est possible de bouger le menu ou de zoom +/-
- [ ] Les Npc sacade, viens du problème de synchro des coordonnée (Probablement pas du tout )


## Idée en vrac !

- Faire tout en style dessin

## Problématique à garder à l'esprit

- Les sprites doivent pouvoir être changer de couleur. Ou à minima un triangle a poser à l'interieur
- La rotation de la grue n'est pas synchronisé entre les clients, car ça risque de faire beaucoup d'échange réseau pour rien (Obligation de repasser par le serveur)