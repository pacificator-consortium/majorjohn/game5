class_name Player

static var player_id_gen: int = 0

var player_id: int
var peer_id: int
var name: String
var color: Color
var ships: Array = []
var score: int

func _init(id_param: int, name_param: String, color_param: Color):
	player_id_gen += 1
	self.player_id = player_id_gen
	self.peer_id = id_param
	self.name = name_param
	self.color = color_param
	self.score = 0

	
func add_ship(ship_node: PlayerShip):
	ship_node.ship_destroy.connect(func(): ships.erase(ship_node))
	ships.append(ship_node)
	
func to_dict_dto() -> Dictionary:
	return { player_id = self.player_id, name = self.name, color = self.color.to_html(), score = self.score } 
	
func to_json_persist() -> Dictionary:
	return { 
		"player_id": self.player_id,
		"peer_id": self.peer_id,
		"name": self.name,
		"color": self.color.to_html(),
		"score": self.score 
	} 
	
static func from_json_persist(player_dict: Dictionary) -> Player:
	var player = Player.new(player_dict["peer_id"], player_dict["name"], Color.html(player_dict["color"]))
	player.player_id = player_dict["player_id"]
	player.score = player_dict["score"]
	return player
