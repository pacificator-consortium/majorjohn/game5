class_name Faction



var faction_id: int
var name: String
var color: Color
var planets: Array = []

func _init(faction_id_param: int, name_param: String, color_param: Color):
	self.faction_id = faction_id_param
	self.name = name_param
	self.color = color_param

	
func to_json_persist() -> Dictionary:
	return { 
		"id": self.faction_id,
		"name": self.name,
		"color": self.color.to_html(),
	} 
	
static func from_json_persist(faction_dict: Dictionary) -> Faction:
	var faction = Faction.new(faction_dict["id"], faction_dict["name"], Color.html(faction_dict["color"]))
	return faction
