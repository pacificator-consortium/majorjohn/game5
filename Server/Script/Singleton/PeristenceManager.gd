extends Node

@onready var ship_multiplayer_spawner_node: MultiplayerSpawner = $/root/MainRootGame/MainMultiplayerSpawner
@onready var planet_multiplayer_spawner_node: PlanetMultiplayerSpawner = $/root/MainRootGame/PlanetMultiplayerSpawner
const SAVE_FILE_PATH: String = "user://savegame.save"

func save_game():
	print("save_game")
	var save_game_file = FileAccess.open(SAVE_FILE_PATH, FileAccess.WRITE)
	var save_ships_nodes = get_tree().get_nodes_in_group("Persist_Ships")
	var save_planets_nodes = get_tree().get_nodes_in_group("Persist_Planets")
	
	var ships = save_ships_nodes.filter(func(node): return node.has_method("to_json_persist")).map(func(node): return node.call("to_json_persist"))
	var players = PlayerManager.players.map(func(player): return player.to_json_persist())
	var planets = save_planets_nodes.filter(func(node): return node.has_method("to_json_persist")).map(func(node): return node.call("to_json_persist"))
	var factions = FactionManager.factions.map(func(faction): return faction.to_json_persist())

	var full_game = {
		"ships": ships,
		"players": players,
		"planets": planets,
		"factions": factions,
		"players_counter": Player.player_id_gen
	}
	
	save_game_file.store_line(JSON.stringify(full_game))
	
func load_game():
	print("load_game")
	if FileAccess.file_exists(SAVE_FILE_PATH):
		var save_file = FileAccess.open(SAVE_FILE_PATH, FileAccess.READ)
		var json = JSON.new()
		var json_string = save_file.get_line()
		var parse_result = json.parse(json_string)
		if not parse_result == OK:
			print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())
		else :
			var node_data = json.get_data()
			load_factions(node_data)
			load_players(node_data)
			load_ships(node_data)
			load_global_data(node_data)
			load_planet(node_data)

func load_players(json: Dictionary):
	for player_dict in json["players"]:
		var player = Player.from_json_persist(player_dict)
		PlayerManager.players.append(player)

func load_ships(json: Dictionary):
	for ships_dict in json["ships"]:
		ship_multiplayer_spawner_node.load_spawn_ship(ships_dict)

func load_planet(json: Dictionary):
	for planet_dict in json["planets"]:
		planet_multiplayer_spawner_node.load_spawn_planet(planet_dict)
		
func load_factions(json: Dictionary):
	for faction_dict in json["factions"]:
		var faction = Faction.from_json_persist(faction_dict)
		FactionManager.add_faction(faction)
		

func load_global_data(json: Dictionary):
	Player.player_id_gen = json["players_counter"]

# Called when the node enters the scene tree for the first time.
func _ready():
	# TODO : Do more cleaner than timer of 3 seconds. Call_deferred ?
	# The initial problem is when we spawn ship node are null because the MainMultiplayerSpawner is not ready
	await get_tree().create_timer(3).timeout
	load_game()
	create_npc_guardian()
	back_up()

# TODO move this function in a dedicated manager ?
func create_npc_guardian():
	for faction in FactionManager.factions:
		ship_multiplayer_spawner_node.spawn_npc_ship(faction)

func back_up():
	while true:
		await get_tree().create_timer(30).timeout
		save_game()
