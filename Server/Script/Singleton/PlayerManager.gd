extends Node

signal player_to_spawn

var players: Array = []

func get_player(player_name: String):
	for player in players:
		if player.name == player_name:
			return player
	return null
	
func get_player_by_peer_id(peer_id: int):
	for player: Player in players:
		if player.peer_id == peer_id:
			return player
	return null

## After this methods, the player is fully connected
@rpc("any_peer", "call_remote", "reliable")
func connect_player(id: int, player_name: String, player_color: Color) -> void:
	print("connect_player")
	var player = self.get_player(player_name)
	if player != null:
		player.peer_id = id
		for ship_node in player.ships:
			ship_node.owner_id = player.peer_id
	else:
		player = Player.new(id, player_name, player_color)
		players.append(player)
		player_to_spawn.emit(player)
	send_new_player(player)
	send_player_list(player.peer_id)
	if player.ships.is_empty():
		player_is_dead.rpc_id(player.peer_id)
		

func send_player_list(peer_id: int):
	var players_dto = players.map(func(p: Player): return p.to_dict_dto())
	var json = JSON.stringify(players_dto)
	print(json)
	populate_player_list.rpc_id(peer_id, json)
	
func send_new_player(player: Player):
	new_player_connect.rpc(JSON.stringify(player.to_dict_dto()))
	
func send_player_update(player: Player):
	update_player.rpc(JSON.stringify(player.to_dict_dto()))
	
## Called when the player with peer_id as destroy a ship
func update_score(peer_id: int):
	for player in players.filter(func(p): return p.peer_id == peer_id):
		player.score += 1
		update_player.rpc(JSON.stringify(player.to_dict_dto()))

@rpc("authority", "call_remote", "reliable")
func populate_player_list(_json_string: String) -> void:
	print("populate_player_list rpc")
	pass
	
@rpc("authority", "call_remote", "reliable")
func player_is_dead() -> void:
	print("player_is_dead rpc")
	pass

@rpc("any_peer", "call_remote", "reliable")
func new_player_connect(_json_string: String) -> void:
	print("new_player_connect rpc")
	pass

@rpc("any_peer", "call_remote", "reliable")
func update_player(_json_string: String) -> void:
	print("update_player rpc")
	pass
	
	

