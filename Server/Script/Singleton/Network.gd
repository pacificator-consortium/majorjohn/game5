extends Node


func _ready():
	print("_ready Network")
	print("Start Server")
	var peer = ENetMultiplayerPeer.new()
	const port: int = 5555
	peer.create_server(port)
	print("Start server on port " + str(port))
	print("Connection Status: " + str(peer.get_connection_status()))
	if peer.get_connection_status() == MultiplayerPeer.CONNECTION_DISCONNECTED:
		OS.alert("Failed to connect.")
		return
	multiplayer.peer_connected.connect(_client_connected)
	multiplayer.peer_disconnected.connect(_client_disconnected)
	multiplayer.multiplayer_peer = peer
	print("Server Started")
	

func _client_connected(_peer):
	print("New client connected peerId: " + str(_peer))
	
func _client_disconnected(_peer):
	print("Client disconnected peerId: " + str(_peer))
	
@rpc("any_peer", "call_remote", "reliable")
func check_player_exist_response(_player_color: Color) -> void:
	print("check_player_exist_response rpc")
	pass


@rpc("any_peer", "call_remote", "reliable")
func check_player_exist(player_name: String) -> void:
	print("check_player_exist")
	var player: Player = PlayerManager.get_player(player_name)
	if player != null:
		check_player_exist_response.rpc(player.color)
	else:
		check_player_exist_response.rpc(null)

