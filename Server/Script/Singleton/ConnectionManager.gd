extends Node

@rpc("any_peer", "call_remote", "reliable")
func check_version(version: String) -> void:
	var peer_id = multiplayer.get_remote_sender_id()
	var server_version = ConfigReader.load_version()
	if server_version == version:
		check_version_response.rpc("")
	else:
		print("Unexpected client version peer_id: %s expected version: %s current client version: %s" % [str(peer_id), server_version, version])
		check_version_response.rpc(server_version)
		kill_socket(peer_id)


@rpc("any_peer", "call_remote", "reliable")
func check_version_response(_player_color: Color) -> void:
	print("check_player_exist_response rpc")
	pass

@rpc("any_peer", "call_remote", "reliable")
func validate_token_response(_is_valid_token: bool) -> void:
	print("validate_token_response rpc")
	pass

@rpc("any_peer", "call_remote", "reliable")
func validate_token(jwt_token: String) -> void:
	var peer_id = multiplayer.get_remote_sender_id()
	if JwtUtils.validate_jwt(jwt_token):
		validate_token_response.rpc(true)
	else:
		validate_token_response.rpc(false)
		kill_socket(peer_id)

@rpc("any_peer", "call_remote", "reliable")
func get_new_token(player_name: String, player_color: Color) -> void:
	if PlayerManager.get_player(player_name) == null:
		var jwt_token = JwtUtils.generate_jwt(player_name, player_color)
		get_new_token_response.rpc(jwt_token)
	else:
		get_new_token_response.rpc(null)
		
	
@rpc("any_peer", "call_remote", "reliable")
func get_new_token_response(_jwt_token: String) -> void:
	print("validate_token_response rpc")
	pass

func kill_socket(peer_id: int):
	#TODO find nicer way (Here, we need to wait the end of check_version_response to kill the socket)
	await get_tree().create_timer(3).timeout
	multiplayer.multiplayer_peer.disconnect_peer(peer_id, false)
