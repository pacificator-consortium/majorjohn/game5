class_name FactionManager


static var factions: Array = []


static func add_faction(faction: Faction):
	factions.append(faction)

static func get_spawn_point(player: Player) -> Vector2:
	return get_faction(player.color).planets[0].position
	
static func get_faction(color: Color) -> Faction:
	for faction: Faction in factions:
		if faction.color == color:
			return faction
	return null

