class_name JwtUtils


const HEADER = {
		"alg": "HS256",
		"typ": "JWT"
	}


static func generate_key():
	return Crypto.new().generate_rsa(2048)

# Fonction pour générer un JWT
static func generate_jwt(player_name: String, player_color: Color) -> String:
	var payload = {
		"player_name": player_name,
		"player_color": player_color.to_html(),
		"iat": Time.get_unix_time_from_system()
	}
	var header_encoded = _base64_url_encode(JSON.stringify(HEADER))
	var payload_encoded = _base64_url_encode(JSON.stringify(payload))
	var signature = _generate_signature(header_encoded, payload_encoded)

	var token = header_encoded + "." + payload_encoded + "." + signature
	print("Generated token " + token)
	return token

# Fonction pour générer la signature
static func _generate_signature(header_encoded: String, payload_encoded: String) -> String:
	var data = header_encoded + "." + payload_encoded
	print("Data to sign: " + data)
	var crypto: Crypto = Crypto.new()
	#var key: CryptoKey = CryptoKey.new()
	
	#var error = key.load_from_string(ConfigReader.load_private_key())
	var signature: PackedByteArray = crypto.sign(HashingContext.HASH_SHA256, data.sha256_buffer(), ConfigReader.load_private_key())
	#return _base64_url_encode(signature.get_string_from_utf8())
	return Marshalls.raw_to_base64(signature)

# Fonction pour encoder en base64 URL
static func _base64_url_encode(data: String) -> String:
	var base64 = Marshalls.utf8_to_base64(data)
	return base64
	#return base64.replace("+", "-").replace("/", "_").replace("=", "")
	
	
static func decode_jwt_payload(jwt_token: String) -> Dictionary:
	var parts = jwt_token.split(".")
	if parts.size() != 3:
		print("Le token JWT n'est pas valide.")
		return {}
	
	var payload_encoded = parts[1]
	var payload_json = _base64_url_decode(payload_encoded)
	var payload_dict = {}
	var result = JSON.new().parse(payload_json)
	if result.error == OK:
		payload_dict = result.result
	else:
		print("Impossible de parser le payload JSON.")
	return payload_dict

# Fonction pour décoder le base64 URL vers une chaîne
static func _base64_url_decode(data: String) -> String:
	#var base64 = data.replace("-", "+").replace("_", "/")
	
	# Ajouter les caractères de remplissage manquants (=) pour être un multiple de 4
	#while base64.length() % 4 != 0:
		#base64 += "="
	
	#return base64.decode_base64()
	return Marshalls.base64_to_utf8(data)

static func validate_jwt(jwt_token: String) -> bool:
	var parts = jwt_token.split(".")
	if parts.size() != 3:
		print("Le token JWT n'est pas valide.")
		return false

	var header_encoded = parts[0]
	var payload_encoded = parts[1]
	var signature_provided = parts[2]

	# Recrée la signature
	var signature_recreated = _generate_signature(header_encoded, payload_encoded)
	
	print("signature_recreated: " + str(signature_recreated))
	print("signature_recreated: size: " + str(signature_recreated.length()))
	print("signature_provided : " + str(signature_provided))
	print("signature_provided : size: " + str(signature_provided.length()))
	
	# Compare la signature fournie avec la signature recréée
	print("result: " + str(str(signature_provided) == str(signature_recreated)))
	return str(signature_provided) == str(signature_recreated)
