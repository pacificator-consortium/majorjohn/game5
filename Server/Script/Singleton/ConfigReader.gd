class_name ConfigReader

const PRIVATE_KEY_PATH: String = "user://priv.pem"
static func load_version() -> String:
	var file = FileAccess.open("res://Config/version.txt", FileAccess.READ)
	var version = file.get_as_text()
	return version

static func load_private_key() -> CryptoKey:
	var crypto_key:CryptoKey
	if FileAccess.file_exists(PRIVATE_KEY_PATH):
		crypto_key = CryptoKey.new()
		crypto_key.load(PRIVATE_KEY_PATH)
	else:
		crypto_key = JwtUtils.generate_key()
		save_private_key(crypto_key)
	return crypto_key
		

static func save_private_key(crypto_key: CryptoKey):
	crypto_key.save(PRIVATE_KEY_PATH)

