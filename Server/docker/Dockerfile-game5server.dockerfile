# === Avec une image pre-process ===
FROM barichello/godot-ci:4.2.2 AS build

WORKDIR /app
COPY . .
RUN mkdir -p Dist
RUN godot --headless --export-debug "server_linux" Dist/game5_server.x86_64

# === Final image ===
FROM debian:12.6 AS runtime

WORKDIR /app

COPY --from=build /app/Dist/* .

RUN apt-get update
RUN apt-get install -y fontconfig

# Here to show files in log
RUN ls -al

CMD ./game5_server.sh --headless