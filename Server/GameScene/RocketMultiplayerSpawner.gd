extends MultiplayerSpawner

const RocketScene = preload("res://GameScene/Prefab/Weapon/Rocket.tscn")

func _enter_tree() -> void:
	spawn_function = _spawn_rocket
	
func _spawn_rocket(_id: int):
	print("_spawn_rocket")
	var rocket_node := RocketScene.instantiate()
	return rocket_node


@rpc("any_peer", "call_remote", "reliable")
func add_rocket(direction: Vector2, starting_position: Vector2, ship_rotation: float) -> void:
	print("add_rocket")
	var rocket_node = self.spawn(1)
	rocket_node.init(direction, starting_position, ship_rotation, multiplayer.get_remote_sender_id())
