extends Area2D

@export var owner_id: int
@export var faction_id: int

func init(starting_point: Vector2, owner_id_param: int):
	owner_id = owner_id_param
	position = starting_point
	visible = true
