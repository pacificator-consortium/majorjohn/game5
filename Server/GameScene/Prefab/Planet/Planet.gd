extends Area2D

class_name Planet

@onready var sprite_node: Sprite2D = $PlanetSprite2D
@export var planet_id: int

func to_json_persist() -> Dictionary:
	return {
		"id" : planet_id,
		"pos_x" : position.x,
		"pos_y" : position.y,
		"self_modulate" : sprite_node.self_modulate.to_html(),
	}
