extends Area2D

class_name PlayerShip

signal ship_destroy

@onready var sprite_node: Sprite2D = $PlayerShipSprite2D

@export var speed: int = 400
## Peer id of the owner
@export var owner_id: int
@export var player_id: int

@export var destination: Vector2

# == Attribut NPC ==
var angle: float = 0.0
var center: Vector2
var radius: float = 300
var angular_speed = speed / radius

func _process(delta):
	if player_id == -1:
		#NPC
		angle += delta * angular_speed
		position = center + Vector2(cos(angle), sin(angle)) * radius
		rotation = angle + PI / 2
	else:
		if destination != null and position != destination:
			look_at(destination)
			position = position.move_toward(destination, delta * speed)
		
@rpc("any_peer", "call_remote", "reliable")
func set_destination(destination_param: Vector2):
	destination = destination_param

func _on_area_entered(area):
	if "is_weapon" in area and area.is_weapon and area.owner_id != owner_id:
		ship_destroy.emit()
		PlayerManager.update_score(area.owner_id)
		area.queue_free()
		queue_free()

func to_json_persist() -> Dictionary:
	return {
		"pos_x" : position.x,
		"pos_y" : position.y,
		"rotation" : rotation,
		"self_modulate" : sprite_node.self_modulate.to_html(),
		"owner_id" : owner_id
	}
