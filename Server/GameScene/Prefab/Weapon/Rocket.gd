extends Area2D

## Rocket design:
## First the rocket follow a vector aligned to the rotation of the launcher during phase_0_direction_DIRECTION_IN_SECOND
## After the timer, the rocket use the direction vector.
## The rocket disaper when there is no more fuel
## The fuel consumption is equals to the number of pixel travelled
class_name Rocket

# === Const ===
# The rocket speed
@export var SPEED_PIXEL_SECOND = 400
# The default fuel capacity
@export var DEFAULT_FUEL_CAPACITY = 3000
# The rocket use the phase_0_direction vector during the first x second
@export var phase_0_direction_DIRECTION_IN_SECOND: int = 2

# === Attribute ===
@onready var sprite_node: Sprite2D = $Sprite2D
@export var owner_id: int
# Need for the collision system
var is_weapon = true
# The fuel left
var fuel_left: float = DEFAULT_FUEL_CAPACITY
# The initial vector of the rocket (Align to the launcher) 
var phase_0_direction: Vector2
# The phase 2 vector (after the curve)
var phase_2_direction: Vector2
# Temporary cumulate of the delta to switch phase 0 to phase 1
var tmp_delta_cumulate: float = 0.0

## 0 = Accelerate forward
## 1 = Curve
## 2 = Continue straight ahead in the direction of the last vector
var phase: int = 0

# target of the rocket (Player mouse position when launching the rocket)
var target: Vector2

# === Stuff for bezier curve ===
var starting_point: Vector2
var control_point: Vector2
var bezier_start_position: Vector2
var interpolation_t: float = 0.0
# Bezier curve length (Use full to compute speed rated to pixel)
var bezier_length: float



func _process(delta: float):
	if fuel_left <= 0:
		queue_free()
	else:
		if phase == 0:
			move_to(phase_0_direction, delta)
			tmp_delta_cumulate += delta
			# Change phase after X second
			if tmp_delta_cumulate > phase_0_direction_DIRECTION_IN_SECOND:
				phase = 1
				bezier_start_position = position
				var curve_amount = abs(angle_between_segments(starting_point, bezier_start_position, target) * 10)
				control_point = calculate_control_point(bezier_start_position, target, curve_amount)
				bezier_length = estimate_bezier_length(bezier_start_position, control_point, target, 10) 
		elif phase == 1:
			move_along_bezier(delta)
			# If we are close to the target stop the curve.
			if position.distance_to(target) < 20:
				phase_2_direction = position.direction_to(target)
				phase = 2
		elif phase == 2:
			move_to(phase_2_direction, delta)


func angle_between_segments(A: Vector2, B: Vector2, C: Vector2) -> float:
	var v1 = (B - A).normalized()  # Vecteur du segment AB
	var v2 = (C - B).normalized()  # Vecteur du segment BC
	return  rad_to_deg(v1.angle_to(v2))  # Retourne l'angle en radians

func estimate_bezier_length(A: Vector2, B: Vector2, C: Vector2, segments: int) -> float:
	var length = 0.0
	var prev_point = A
	
	for i in range(1, segments + 1):
		var t = i / float(segments)
		var p1 = lerp(A, B, t)
		var p2 = lerp(B, C, t)
		var current_point = lerp(p1, p2, t)
		length += prev_point.distance_to(current_point)
		prev_point = current_point
	
	return length

func move_along_bezier(delta: float):
	var speed_t = SPEED_PIXEL_SECOND / bezier_length
	interpolation_t += delta * speed_t 
	var p1 = lerp(bezier_start_position, control_point, interpolation_t)
	var p2 = lerp(control_point, target, interpolation_t)
	global_position = lerp(p1, p2, interpolation_t)
	look_at(p2) 

## Calculate the control point for the bezier curve
func calculate_control_point(A: Vector2, B: Vector2, curve_amount: float) -> Vector2:
	var midpoint = (A + B) / 2
	var direction = B - A
	var perpendicular = (position - starting_point).normalized()
	return midpoint + perpendicular * curve_amount

## Move to a direction using delta
## Update the fuel consumption
func move_to(direction: Vector2, delta: float):
	var distance = delta * SPEED_PIXEL_SECOND
	fuel_left -= distance
	position += direction * distance

func init(target_param: Vector2, starting_point_param: Vector2, ship_rotation: float, owner_id_param: int):
	starting_point = starting_point_param
	owner_id = owner_id_param
	phase_0_direction = Vector2.RIGHT.rotated(ship_rotation)
	target = target_param
	position = starting_point_param
	rotation = ship_rotation
	visible = true
