extends MultiplayerSpawner

const PlayerScene = preload("res://GameScene/Prefab/PlayerShip/PlayerShip.tscn")


func _enter_tree() -> void:
	spawn_function = _spawn_ship

func _spawn_ship(id: int):
	print("_spawn_ship id: " + str(id))
	var ship_node = PlayerScene.instantiate()
	return ship_node

func spawn_ship(player: Player) -> void:
	print("New Ship To Spawn " + str(player.peer_id) + " " + player.name)
	var ship_node: PlayerShip = self.spawn(player.peer_id)
	ship_node.sprite_node.self_modulate = player.color
	ship_node.owner_id = player.peer_id
	ship_node.player_id = player.player_id
	ship_node.position = FactionManager.get_spawn_point(player)
	ship_node.destination = ship_node.position 
	player.add_ship(ship_node)
	
func spawn_npc_ship(faction: Faction) -> void:
	print("New npc ship to spawn ")
	var ship_node: PlayerShip = self.spawn(-1)
	ship_node.sprite_node.self_modulate = faction.color
	ship_node.owner_id = -1
	ship_node.player_id = -1
	ship_node.position = faction.planets[0].position
	ship_node.center = faction.planets[0].position 
	
## Method calls after server start
## Load saved ships
func load_spawn_ship(ship_dict: Dictionary):
	var player = PlayerManager.get_player_by_peer_id(ship_dict["owner_id"])
	if(player == null):
		print("Something went wrong durring loading ship: " + JSON.stringify(ship_dict))
	else:
		var player_node = self.spawn(ship_dict["owner_id"])
		player_node.sprite_node.self_modulate = Color.html(ship_dict["self_modulate"])
		player_node.owner_id = ship_dict["owner_id"]
		player_node.player_id = player.player_id
		player_node.destination = Vector2(ship_dict["pos_x"],ship_dict["pos_y"])
		player_node.position = Vector2(ship_dict["pos_x"],ship_dict["pos_y"])
		player_node.rotation = ship_dict["rotation"]
		player.add_ship(player_node)

@rpc("any_peer", "call_remote", "reliable")
func spawn_new_ship() -> void:
	print("spawn_new_ship")
	var player: Player = PlayerManager.get_player_by_peer_id(multiplayer.get_remote_sender_id())
	spawn_ship(player)

func _ready():
	PlayerManager.player_to_spawn.connect(spawn_ship)
