extends MultiplayerSpawner

class_name PlanetMultiplayerSpawner

const PlanetScene = preload("res://GameScene/Prefab/Planet/Planet.tscn")

func _enter_tree() -> void:
	spawn_function = _spawn_planet

func _spawn_planet(id: int) -> Planet: 
	print("_spawn_planet id: " + str(id))
	return PlanetScene.instantiate()
	
## Method calls after server start
## Load saved planets
func load_spawn_planet(planet_dict: Dictionary):
	var planet_node: Planet = self.spawn(planet_dict["id"])
	planet_node.planet_id = planet_dict["id"]
	planet_node.sprite_node.self_modulate = Color.html(planet_dict["self_modulate"])
	planet_node.position = Vector2(planet_dict["pos_x"],planet_dict["pos_y"])
	var faction: Faction = FactionManager.get_faction(planet_node.sprite_node.self_modulate)
	faction.planets.append(planet_node) 
